# %%

from auror_gui import AurorGUI
import pyqtgraph as pg
import rpspy
from process import Process
import os

app = pg.mkQApp()
#import pyqtgraph.console
#cw = pyqtgraph.console.ConsoleWidget()
# cw.show()
# cw.catchNextException()
# win = RelativityGUI()
win = AurorGUI()
win.setWindowTitle("Auror 2022")
win.show()
win.resize(1100, 700)

current_process = Process(70031, "D:\\Downloads\\70031")

win.k_hfs.plot(current_process.bands[0].amplitude[0])
win.k_hfs.setLimits(xMin=0, xMax=1023, yMin=0, yMax=2 **
                    12-1, maxXRange=1024, maxYRange=2**12)
win.k_hfs.setRange(xRange=(0, 1023), yRange=(0, 2**12-1))

current_process


pg.exec()


print("Auror closed")

# %%
