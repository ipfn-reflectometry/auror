import rpspy as rp
from dataclasses import dataclass
import numpy as np


# Spectrogram options
_spectrogram_options = {
    'K': {'nperseg': 128, 'noverlap': 96, 'nfft': 4096},
    'Ka': {'nperseg': 128, 'noverlap': 96, 'nfft': 4096},
    'Q': {'nperseg': 128, 'noverlap': 96, 'nfft': 4096},
    'V': {'nperseg': 64, 'noverlap': 48, 'nfft': 4096},
}


def normalize(x, n=1, axis=-1):
    return x / np.max(np.abs(x), axis=axis) * n


@dataclass
class Band:
    name: str
    side: str
    dtype: str
    frequency: np.ndarray = 0
    amplitude: np.ndarray = 0
    nperseg: int = 0
    noverlap: int = 0
    nfft: int = 0
    x: np.ndarray = 0
    y: np.ndarray = 0
    spectrogram: np.ndarray = 0
    bg_spectrogram: np.ndarray = 0
    spectrogram_corrected: np.ndarray = 0
    x_sampled: np.ndarray = 0
    y_sampled: np.ndarray = 0
    spectrogram_sampled: np.ndarray = 0
    dispersion: np.ndarray = 0
    sweep_rate: float = 0

    def __init__(self, name, side, dtype):
        self.name = name
        self.side = side
        self.dtype = dtype

        self.nperseg = _spectrogram_options[self.name]['nperseg']
        self.noverlap = _spectrogram_options[self.name]['noverlap']
        self.nfft = _spectrogram_options[self.name]['nfft']


class Process():
    def __init__(self, shot: int, shotfile_dir) -> None:
        self.shot = shot
        self.shotfile_dir = shotfile_dir

        # Read meta-data
        sampling_frequency = rp.get_sampling_frequency(
            self.shot, self.shotfile_dir)

        # Choose time instant and number of bursts --------------------
        time_instant = 0.1
        sweep = rp.get_timestamps(self.shot, self.shotfile_dir, time_instant)
        burst = 21

        # Read the raw data -------------------------------------------
        self.bands = [Band(name, side, dtype) for side in ['HFS', 'LFS'] for name, dtype in [
            ('K', 'real'),
            ('Ka', 'real'),
            ('Q', 'real'),
            ('V', 'complex')
        ]]

        for band in self.bands:

            # get the signal linearization curve -------------------------
            # frequency = rp.get_linearization(shot_linearization, sweep_linearization, band.name, shotfile_dir=linearization_shotfile_dir)
            frequency = rp.get_linearization(
                self.shot, sweep, band.name, shotfile_dir=self.shotfile_dir)

            signal = rp.get_band_signal(
                self.shot, self.shotfile_dir, band.name, band.side, band.dtype, burst, sweep)

            # Linearize the signals ---------------------------------------
            for i in range(burst):
                frequency_linear, signal[i] = rp.linearize(
                    frequency, signal[i])

            # Save the signal -------------------------
            band.frequency = frequency_linear
            band.sweep_rate = (
                band.frequency[1] - band.frequency[0]) * sampling_frequency
            band.amplitude = signal
